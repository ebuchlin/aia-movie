#!/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(name='aiamovie', author='Éric Buchlin, Baptiste Meylheuc',
      packages=find_packages())
