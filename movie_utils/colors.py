import numpy as np

from matplotlib.pyplot import get_cmap
import colorsys
import astropy.units as u
from sunpy.visualization.colormaps.color_tables import aia_color_table


def convert_k_to_rgb(t):
    """
    Empirical RGB for given temperature

    Following https://gist.github.com/petrklus/b1f427accdf7438606a6
    derived from http://www.tannerhelland.com/4435/convert-temperature-rgb-algorithm-code/

    Parameters
    ----------
    t: float
        Temperature

    Return
    ------
    array
        RGB values (in range [0,1])
    """
    t = np.clip(t, 1000., 40000.) / 100.
    # red
    if t <= 66.:
        r = 1.
    else:
        r = (329.698727446 * (t - 60.) ** (-0.1332047592)) / 255.
    # green
    if t <= 66.:
        g = (99.4708025861 * np.log(t) - 161.1195681661) / 255.
    else:
        g = (288.1221695283 * (t - 60.) ** (-0.0755148492)) / 255.
    # blue
    if t >= 66.:
        b = 1.
    elif t <= 19.:
        b = 0.
    else:
        b = (138.5177312231 * np.log(t - 10.) - 305.0447927307) / 255.
    return np.clip(np.array([r, g, b]), 0., 1.)


def apply_color_model(data, parameters):
    """
    Apply color model to data array

    Parameters
    ----------
    data: numpy.ndarray
        Data array, dimensions (channel, y, x), values between 0 and 1
    parameters: Parameters
        Parameters

    Return
    ------
    image: numpy.ndarray
        RGB image array, dimensions (color channel, y, x), values between 0 and 1 TODO
    """
    functions = {
        'rgb': apply_rgb_model,
        'map': apply_color_map,
        'gray': lambda d, p: apply_color_map(d, p, color_map='gray'),
        'pm': apply_planck_mapping
    }
    return functions[parameters.color_model](data, parameters)


def apply_rgb_model(data, parameters):
    return data


def apply_color_map(data, parameters, color_map=None):
    if color_map is None:
        color_map = aia_color_table(int(parameters.channels[0]) * u.angstrom) 
    elif type(color_map) is str:
        color_map = get_cmap(color_map)
    color_mapped = color_map(data[0, ...])
    if data.ndim > 1:
        return np.transpose(color_mapped[..., :3], axes=(2, 0, 1))
    else:
        return np.array(color_mapped[:3])


def apply_planck_mapping(data, parameters):
    logts = {'131': 7.0, '171': 5.8, '193': 6.1, '211': 6.3, '304': 4.5, '335': 6.4}
    logts = np.array([logts[channel] for channel in parameters.channels])
    ts = 4.3 ** logts   # log10 T remapped to log4.3 T
    # black-body radiation for each channel, then mixed between channels
    rgbbb = np.array([convert_k_to_rgb(t) for t in ts]).T
    # TODO go back to original gamma (whatever this means?) to compute PM color?
    image = np.tensordot(rgbbb, data, axes=1)
    if data.ndim > 1:   # only on image, not on single pixel
        # white balance / normalize each channel to maximum
        axes_im = tuple(range(1, image.ndim))
        rgb_max = np.nanmax(image, axis=axes_im)
        image /= np.expand_dims(rgb_max, axis=axes_im)
        # increase saturation (by arbitrary factor)
        saturation_factor = 2.
        image_hsv = np.array(list(
            map(colorsys.rgb_to_hsv, image[0, ...].flat, image[1, ...].flat, image[2, ...].flat)
        ))
        image_hsv[:, 1] *= saturation_factor
        image_rgb = np.array(list(
            map(colorsys.hsv_to_rgb, image_hsv[:, 0], image_hsv[:, 1], image_hsv[:, 2])
        ))
        image = image_rgb.T.reshape(image.shape)
    image = np.clip(image, 0., 1.)
    return image
