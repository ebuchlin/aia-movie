import astropy.units as u
import numpy as np
from sitools2 import SdoClientMedoc


def get_data_list(parameters):
    """
    Search in database and get list of data files

    Parameters
    ----------
    parameters: Parameters
        Parameters

    Return
    ------
    dict
        Dictionary (one item per band) of database entries
    """
    # Search available files
    client = SdoClientMedoc()
    data_list0 = dict()
    all_t_obs = dict()
    for channel in parameters.channels:
        print('Searching for', channel)
        results = client.search(
            dates=list(parameters.date_range.to_datetime()),
            waves=[channel],
            cadence=[parameters.search_cadence],
            nb_res_max=20000
        )
        # get metadata in blocks (not possible for all at once because of URL length limitation)
        aia_keywords = list()
        chunk_size = 500
        for chunk in (results[pos:pos + chunk_size] for pos in range(0, len(results), chunk_size)):
            keywords = client.sdo_metadata_search(chunk, series='aia.lev1', keywords=['quality', 't_obs'])
            aia_keywords.extend(keywords)
        t_obs = [kw['t_obs'] for kw in aia_keywords]
        # select good quality data, for which data are present at IAS
        quality = [kw['quality'] for kw in aia_keywords]
        ias_location = [result.ias_location for result in results]
        where = [(q == 0) and (l != '') for (q, l) in zip(quality, ias_location)]
        data_list0[channel] = [result for result, w in zip(results, where) if w]
        all_t_obs[channel] = np.array([t_obs for t_obs, w in zip(t_obs, where) if w])
    # select files for times corresponding to movie images
    tobs0 = np.median([all_t_obs[channel][0] for channel in parameters.channels])
    tobs1 = np.median([all_t_obs[channel][-1] for channel in parameters.channels])
    cadence_seconds = parameters.cadence.to(u.s).value
    time_targets = np.arange(tobs0, tobs1 + cadence_seconds / 2, cadence_seconds)
    data_list = list()
    for time_target in time_targets:
        channel_list = list()
        for channel in parameters.channels:
            closest = np.argmin(np.abs(all_t_obs[channel] - time_target))
            channel_list.append(
                {
                    'url': data_list0[channel][closest].url,
                    'ias_location': data_list0[channel][closest].ias_location,
                    't_obs': all_t_obs[channel][closest]
                }
            )
        data_list.append(channel_list)
    return data_list


class DataList(list):
    """
    List of data to be used.
    Each item corresponds to an output image, and is a list.
    Each item of this list corresponds to a channel.
    """
    def __init__(self, parameters, *args):
        list.__init__(self, *args)
        self.extend(get_data_list(parameters))
