from pathlib import Path
import numpy as np
from PIL import Image, ImageFont, ImageDraw

from astropy.time import Time, TimeDelta
from .colors import apply_color_model


# From Gabriel Pelouze:
def sdo_time_to_utc(sdo_time):
    """
    Convert SDO time as in the MEDOC database (seconds since
    1977-01-01T00:00:00TAI) to UTC datetime.
    """
    t_ref = Time('1977-01-01T00:00:00', scale='tai')
    t_tai = t_ref + TimeDelta(sdo_time, format='sec', scale='tai')
    return t_tai.utc.datetime


def find_font_file():
    font_paths = [
        '/usr/share/fonts/truetype/droid/',
        '/home/ebuchlin/tmp/',
        '/usr/share/texlive/texmf-dist/fonts/truetype/public/droid/',
        '/usr/share/texlive/texmf-dist/fonts/truetype/ascender/droid/droidsans/'
    ]
    font_name = 'DroidSans-Bold.ttf'
    for font_path in font_paths:
        font_file = Path(font_path) / font_name
        if font_file.exists():
            return font_file.as_posix()
    raise RuntimeError('No font file found')


class SunImage:
    """
    Image produced from data
    """
    def __init__(self, data, data_item, parameters):
        self.parameters = parameters
        self.image = None
        self.produce_rgb_image(data)
        self.write_timestamp(data_item)

    def produce_rgb_image(self, data):
        self.image = apply_color_model(data.data, self.parameters)
        self.image = Image.fromarray(
            (np.transpose(self.image, axes=(1, 2, 0)) * 255.99)[::-1, ...].astype(np.uint8),
            'RGB'
        )

    def show(self):
        self.image.show()

    def write_timestamp(self, data_item, separate_file=False, timestamp_file=None):
        """
        Write timestamp

        Parameters
        ----------
        data_item: list[dict]
            Metadata corresponding to image
        separate_file: bool
            Write timestamp to separate file instead of writing it on the image itself
        timestamp_file: str
            File name for timestamp image (no extension)
        """
        font_size = max(12, int(self.image.height / 80.))
        font = ImageFont.truetype(find_font_file(), font_size)
        char_width, char_height = font.getsize("0")
        if separate_file:
            timestamp_image = Image.fromarray(
                np.zeros((int(np.ceil(char_height * len(self.parameters.channels) * 1.2)), char_width * 25, 4)), 'RGBA')
            draw = ImageDraw.Draw(timestamp_image, 'RGBA')
        else:
            draw = ImageDraw.Draw(self.image, 'RGBA')
        for i, channel in enumerate(self.parameters.channels):
            text = sdo_time_to_utc(np.round(data_item[i]["t_obs"])).isoformat()
            text = f'{channel}: {text}'  # TODO to ISO8601 (find Gabriel's code for this)
            channel_array = np.zeros((len(self.parameters.channels)))
            channel_array[i] = 1
            color = (apply_color_model(channel_array, self.parameters) * 255.99).astype(np.uint8)
            draw.text((0, i * char_height * 1.2), text, font=font, fill=tuple(color))
        if separate_file:
            timestamp_image.save(f'{timestamp_file}.png')

    def save(self, out_file):
        """
        Write image

        Parameters
        ----------
        out_file: str
            Image file name (no extension)
        """
        self.image.save(f'{out_file}.png')
        # np.save(f'{out_file}.npy', np.array(self.image))

    def save_channels(self, data, out_file, data_range=(0., 1.)):
        # save array for different channels as images for debug
        for i in range(len(self.parameters.channels)):
            image_data = (data.data[i, ...] - data_range[0]) / (data_range[1] - data_range[0])
            image_data = (np.clip(image_data, 0., 1.) * 255.99)[::-1, ...].astype(np.uint8)
            image = Image.fromarray(image_data, 'L')
            image.save(f'{out_file}_{i}.png')
