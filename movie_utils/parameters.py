from dataclasses import dataclass
from typing import Union, Any
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy.time import Time
import astropy.units as u
from sunpy.map.header_helper import make_fitswcs_header
from sunpy.coordinates import frames


@dataclass
class Parameters:
    date_range: tuple  # datetime for start and end of interval
    cadence: Union[str, u.Quantity] = '1h'  # movie cadence
    search_cadence: str = '1h'  # search cadence
    channels: Union[list[str], None] = None  # list of channels to use
    prep_steps: Union[list[str], None] = None  # AIA prep steps
    image_wcs: Union[WCS, None] = None  # output WCS for the movie images
    image_filter: Union[str, None] = None  # image filter to use
    filter_args: Union[dict, None] = None  # arguments for image filter
    image_gamma: float = 2.  # image gamma correction exponent
    color_model: str = 'rgb'  # color model for transforming channel values into colors
    show_caption: bool = True  # show caption in images
    value_ranges: Union[list[Any], None] = None

    def __post_init__(self):
        """
        Validate parameters
        """
        self.validate_date_range()
        self.validate_channels()
        self.validate_cadences()
        self.validate_prep_steps()
        self.validate_wcs()
        self.validate_image_filter()
        self.validate_color_model()

    def validate_date_range(self):
        assert len(self.date_range) == 2
        for date in self.date_range:
            assert type(date) is Time
        assert self.date_range[0] < self.date_range[1]

    def validate_channels(self):
        """
        Get ranges of input values for all channels and get default value ranges
        """
        all_value_ranges = {
            '94': (-.5, 300),
            '131': (-.5, 400),
            '171': (5, 6000),
            '193': (1, 6000),
            '211': (0, 5000),
            '304': (0, 5000),
            '335': (0, 200),
        }
        if self.channels is None:
            self.channels = ['171']
        for channel in self.channels:
            assert channel in all_value_ranges
        if self.value_ranges is None:
            self.value_ranges = [all_value_ranges[channel] for channel in self.channels]

    def validate_cadences(self):
        allowed_cadences = {'12s': 12, '1m': 60, '2m': 120, '5m': 300, '10m': 600, '30m': 1800, '1h': 3600, '2h': 7200,
                            '6h': 21600, '12h': 43200, '1d': 86400}
        if type(self.cadence) is str:
            assert self.cadence in allowed_cadences
            self.cadence = allowed_cadences[self.cadence] * u.s
        assert self.search_cadence in allowed_cadences

    def validate_prep_steps(self):
        allowed_steps = ['deconvolve', 'pointing', 'degradation', 'exposure']
        if self.prep_steps is None:
            self.prep_steps = allowed_steps[1:]
        for step in self.prep_steps:
            assert step in allowed_steps

    def validate_wcs(self):
        if self.image_wcs is None:
            center = SkyCoord(
                0 * u.arcsec, 0 * u.arcsec,
                observer='earth',
                obstime=self.mid_date_range,
                frame=frames.Helioprojective
            )
            self.image_wcs = WCS(
                make_fitswcs_header(
                    data=(1080, 1920),
                    coordinate=center,
                    scale=[1.5, 1.5] * u.arcsec / u.pix,
                )
            )

    def validate_image_filter(self):
        allowed_image_filters = [None, 'unsharp', 'nafe', 'mgn', 'wow']
        assert self.image_filter in allowed_image_filters
        if self.filter_args is None:
            # default filter arguments
            if self.image_filter == 'unsharp':
                x_pixel_size = self.image_wcs.proj_plane_pixel_scales()[0].to('arcsec').value
                aia_pixel_size = .6
                radius_pixels = 1.5
                self.filter_args = {
                    'radius': max(x_pixel_size / aia_pixel_size * radius_pixels, radius_pixels),
                    'amount': 2.,
                }
                print(f'{x_pixel_size=}, {aia_pixel_size=}, {self.filter_args["radius"]=}')
            elif self.image_filter == 'mgn':
                self.filter_args = dict()
            elif self.image_filter == 'wow':
                self.filter_args = {
                    'bilateral': 1,
                    'denoise_coefficients': [5, 2]
                }
            else:
                self.filter_args = dict()

    def validate_color_model(self):
        allowed_color_models = ['rgb', 'map', 'gray', 'pm']
        assert self.color_model in allowed_color_models
        if self.color_model == 'rgb':
            assert len(self.channels) in [1, 3]
        elif self.color_model in ['map', 'gray']:
            assert len(self.channels) == 1

    @property
    def mid_date_range(self):
        """
        Middle of date range
        """
        return self.date_range[0] + \
            (self.date_range[1] - self.date_range[0]) / 2

    def __str__(self):
        return f'''
Parameters:
-----------
From {self.date_range[0]} to {self.date_range[1]}
Cadence: {self.cadence} (search cadence: {self.search_cadence})
Channels: {self.channels}
Image filter {self.image_filter} with arguments {self.filter_args}
Color model: {self.color_model}, gamma {self.image_gamma}
Image {self.image_wcs}
'''
