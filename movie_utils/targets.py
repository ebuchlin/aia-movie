from rich import print

import astropy.units as u
from astropy.time import Time
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from sunpy.map.header_helper import make_fitswcs_header
from sunpy.coordinates import frames


class Targets(dict):
    """
    Suggested date ranges and FOVs for different events
    """

    def __init__(self, name):
        super().__init__()
        functions = {
            'eruption20120831': Targets.eruption20120831
        }
        try:
            self.update(functions[name]())
        except KeyError:
            print(
                f'[red]Error:[/] target {name} not available, available targets include {", ".join(functions.keys())}.')

    @staticmethod
    def eruption20120831():
        """
        2012-08-31 large filament eruption, Cartesian coordinates, full HD
        """
        parameters = dict()
        parameters['date_range'] = Time(['2012-08-31T18:00:00', '2012-08-31T22:00:00'])
        center = SkyCoord(
            -600 * u.arcsec, -500 * u.arcsec,
            observer='earth',
            obstime=Targets.mid_date_range(parameters['date_range']),
            frame=frames.Helioprojective
        )
        parameters['image_wcs'] = WCS(
            make_fitswcs_header(
                data=(1080, 1920),
                coordinate=center,
                scale=[.6, .6] * u.arcsec / u.pix,
            )
        )
        return parameters

    @staticmethod
    def mid_date_range(date_range):
        return date_range[0] + (date_range[1] - date_range[0]) / 2
