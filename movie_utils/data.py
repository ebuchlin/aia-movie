from pathlib import Path

import numpy as np
from parfive import Downloader
from sunpy.map import Map
from sunpy.coordinates import Helioprojective
import aiapy.psf
import aiapy.calibrate

from skimage.filters import unsharp_mask
from sunkit_image.enhance import mgn
from watroo import wow


class Data:
    """
    Data for all channels used to produce an image
    """

    def __init__(self, data_item, parameters):
        self.file_names = None
        self.data = None
        self.parameters = parameters
        self.download_files(data_item)
        self.get_data()
        self.filter_data()

    def download_files(self, data_item):
        """
        Download FITS files (it not already present locally) and update list
        of local files with the downloaded files
        """
        dl = Downloader()
        local_file_names = [
            Path(data_item_channel['ias_location']) / 'S00000' / 'image_lev1.fits'
            for data_item_channel in data_item
        ]
        urls = [data_item_channel['url'] for data_item_channel in data_item]
        dl_indices = list()
        for index, (local_file_name, url) in enumerate(zip(local_file_names, urls)):
            if not local_file_name.exists():
                dl.enqueue_file(url, path="./fits_cache")
                dl_indices.append(index)
        result = dl.download()
        n_retries = 0
        while result.errors and (n_retries < 10):
            print(f'Retrying following error {result.errors}')
            result = dl.retry(result)
            n_retries += 1
        self.file_names = [
            result[i] if i in dl_indices else local_file_names[i]
            for i in range(len(data_item))
        ]

    def get_data(self):
        """
        Read FITS files and get reprojected image data
        """
        data = list()
        for local_filename in self.file_names:
            aia_map = Map(local_filename)
            prep_functions = {
                'deconvolve': aiapy.psf.deconvolve,
                'pointing': aiapy.calibrate.update_pointing,
                'degradation': aiapy.calibrate.correct_degradation,
                'exposure': aiapy.calibrate.normalize_exposure
            }
            for step in self.parameters.prep_steps:
                aia_map = prep_functions[step](aia_map)
            with Helioprojective.assume_spherical_screen(aia_map.observer_coordinate, only_off_disk=True):
                # 'adaptive' uses DeForest (2004) algorithm
                reprojected_map = aia_map.reproject_to(self.parameters.image_wcs, algorithm='adaptive')
            data.append(reprojected_map.data)
        self.data = np.stack(data)

    def normalize_data(self, clip=True, gamma=False):
        """
        Normalize data in each channel between 0 and 1, based on the value ranges parameters
        """
        for i, value_range in enumerate(self.parameters.value_ranges):
            self.data[i, ...] = (self.data[i, ...] - value_range[0]) / (value_range[1] - value_range[0])
        if clip:
            np.clip(self.data, 0., 1., out=self.data)
        if gamma:
            self.data = self.data ** (1 / self.parameters.image_gamma)

    def filter_data(self):
        """
        Enhance data with a filter
        """
        if self.parameters.image_filter is None:
            self.normalize_data(gamma=True)
        elif self.parameters.image_filter == 'unsharp':
            self.data = unsharp_mask(self.data, channel_axis=0, preserve_range=True, **self.parameters.filter_args)
            self.normalize_data(gamma=True)
        elif self.parameters.image_filter == 'nafe':
            raise RuntimeError('Not implemented')
        elif self.parameters.image_filter == 'mgn':
            self.data[np.isnan(self.data)] = 0
            filter_args = self.parameters.filter_args
            self.normalize_data()
            for i in range(len(self.parameters.channels)):
                filter_args.update({'gamma': 1})  # self.parameters.image_gamma})
                self.data[i, ...] = mgn(self.data[i, ...], **filter_args)
        elif self.parameters.image_filter == 'wow':
            self.data[np.isnan(self.data)] = 0
            self.normalize_data()
            for i in range(len(self.parameters.channels)):
                self.data[i, ...], _ = wow(self.data[i, ...], **self.parameters.filter_args)
            vmin, vmax = [-7, 15]  # TODO arbitrary range
            self.data = np.clip((self.data - vmin) / (vmax - vmin), 0., 1.)
        else:
            raise ValueError('Unknown image filter')
