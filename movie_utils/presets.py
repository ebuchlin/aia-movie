from rich import print


class Presets(dict):
    """
    Output parameters presets
    """
    def __init__(self, name):
        super().__init__()
        functions = {
            'rgbwow': Presets.rgbwow,
            'pmwow': Presets.pmwow,
        }
        try:
            self.update(functions[name]())
        except KeyError:
            print(
                f'[red]Error:[/] preset {name} not available, available presets include {", ".join(functions.keys())}.'
            )

    @staticmethod
    def rgbwow():
        """
        (171, 193, 211) RGB mapping with WOW
        """
        return {
            'channels': ['171', '193', '211'],
            'color_model': 'rgb',
            'image_filter': 'wow',
        }

    @staticmethod
    def pmwow():
        """
        Planck mapping with WOW
        """
        return {
            'channels': ['304', '171', '193', '211', '335'],
            'color_model': 'pm',
            'image_filter': 'wow',
        }
