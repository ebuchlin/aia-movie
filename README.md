# Python scripts for SDO/AIA image enhancement and movie creation



## `medocimage`

This is a module implementing different image processing algorithms initially developped for solar physics, namely [NAFE](http://www.zam.fme.vutbr.cz/~druck/Nafe/) and [MGN](http://eagle.imaps.aber.ac.uk/mgn.pro).

MGN and NAFE require the `numpy` and `scipy.ndimage` Python modules.
In addition, NAFE requires the `multiprocessing.Pool` and `functools.partial` modules for parallelization.
These should be part of any scientific Python distribution.


### `medocimage.nafe`

This is a Python and parallel implementation of NAFE, as described in [Druckmüller, M. (2013). A noise adaptive fuzzy equalization method for processing solar extreme ultraviolet images. *Astrophys. J. Supp. Ser.* **207**, 25](http://dx.doi.org/10.1088/0067-0049/207/2/25).

Parameters:

* `image`: Image
* `n`: Size (width) of fuzzy neighborhood
* `a`: Minimum and maximum input values in image
* `b`: Minimum and maximum output values in image
* `w`: Weight of the NAFE-processed image in output image
* `gamma`: Gamma exponent for background image in output image
* `sigma`: Additive noise level (do not amplify noise in input image below this level)
* `nproc`: Number of CPU cores to use.


### `medocimage.mgn`

This is a Python implementation of MGN, as described in [Morgan, H. and Druckmüller, M. (2014). Multi-Scale Gaussian Normalization for Solar Image Processing.  *Sol. Phys.*, **289**, 2945](http://dx.doi.org/10.1007/s11207-014-0523-9).

Parameters:

* `image`: Image
* `a`: Minimum and maximum input values in image
* `b`: Minimum and maximum output values in image
* `w`: Weight of the MGN-processed image in output image (1-h in the reference paper)
* `gamma`: Gamma exponent for background image in output image
* `sigma`: Widths of Gaussian kernels (w in the reference paper)


## `movie_utils`

Python classes for creating a movie from AIA data:

* Select movie parameters (also using pre-selected targets and projections)
* Search and access data
* Normalize data (gamma, clip) and filter data (unsharp, MGN, NAFE, WOW)
* Apply a color scheme (RGB, standard AIA map, gray scale, Planckian mapping)
* Save images (movie frames)

For examples (including generation of a movie from the generated images), please see in `examples/`.


## License

[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html)
