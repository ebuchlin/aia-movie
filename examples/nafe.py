import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from sunpy.map import Map
from sunpy.data.sample import AIA_171_IMAGE
from medocimage import nafe

m = Map(AIA_171_IMAGE)
image = m.data
start_time = datetime.utcnow()
nafe_image = nafe(image, n=41, a=(0., np.percentile(image, 99.5)), b=(0., 1.), w=.2, gamma=.8, sigma=5)
end_time = datetime.utcnow()
print(f'Running time: {(end_time - start_time).total_seconds()}s')
m2 = Map((nafe_image, m.meta))
m2.plot()
plt.show()
