'''
Create a movie from SDO/AIA images.

Parameters are an instance of the Parameters class.
'''
import subprocess
from multiprocessing import Pool
from pathlib import Path
from itertools import cycle
import pickle # to test function pickling

from rich import print
import astropy.units as u
from astropy.time import Time
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from sunpy.map.header_helper import make_fitswcs_header
from sunpy.coordinates import frames

from movie_utils import Parameters, DataList, Data, SunImage, Targets, Presets


def produce_image(i, data_item, parameters):
    output = f'frame-{i:05}'
    if Path(f'{output}.png').exists():
        print(f'[yellow]Skipping frame {i}[/] (file already exists)')
    else:
        print(f'[green]Generating frame {i}[/]')
        data = Data(data_item, parameters)
        image = SunImage(data, data_item, parameters)
        # image.save_channels(data, output)  # only for debug
        image.save(output)


if __name__ == '__main__':
    parameters = Parameters(
        **Targets('eruption20120831'),
        # **Presets('pmwow'),
        channels=['304', '171', '193', '211', '335'],
        color_model='pm',
        image_filter='wow',
        cadence=5*u.min, search_cadence='1m',
    )
    print(parameters)
    data_list = DataList(parameters)
    nproc = 1
    if nproc == 1:
        for i, data_item in enumerate(data_list):
            produce_image(i, data_item, parameters)
            # break  # DEBUG
    else:
        pool = Pool(processes=nproc)
        n = len(data_list)
        pool.starmap(produce_image, zip(range(n), data_list, cycle([parameters])))
        # pool.map(produce_image)

    # Movie encoding:
    # ffmpeg -i frame-%05d.png -vcodec libx264 -preset fast -crf 23 -vf format=yuv420p /tmp/out.mp4
    p = subprocess.Popen(['ffmpeg', '-i', 'frame-%05d.png', '-vcodec', 'libx264', '-preset', 'fast',
                            '-crf', '23', '-vf', 'format=yuv420p', '/tmp/out.mp4'])
    p.wait()

    # adding a silent audio track (required for some video readers):
    # ffmpeg  -ar 44100 -acodec pcm_s16le -f s16le -ac 2 -i /dev/zero -i /tmp/out.mp4 -shortest -vcodec copy -acodec libmp3lame /tmp/out_silent.mp4 -map 1:0 -map 0:0
