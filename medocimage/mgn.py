# -*- coding: utf-8 -*-

'''MGN image processing, Morgan & Druckmüller 2014, http://eagle.imaps.aber.ac.uk/mgn.pro
'''

import numpy as np
import scipy.ndimage


def mgn(image, a=(5., 5000.), b=(0., 1.), w=0.3, gamma=3.2, sigma=[2.5, 5, 10, 20, 40], k=0.7):
    """MGN algorithm (Morgan, H. and Druckmüller, M. Multi-Scale Gaussian
    Normalization for Solar Image Processing.  Sol. Phys., 289, 2945, 2014).

    Parameters
    ----------
    image: ndarray
      Image
    a: tuple
      Minimum and maximum input values in image
    b: tuple
      Minimum and maximum output values in image ([a[0], a[1]] will be scaled to [b[0], b[1]])
    w: float
      Weight of the MGN-processed image in output image
    gamma: float
      Gamma exponent for background image in output image
    sigma: iterable of floats
      Widths of Gaussian kernels

    Return
    ------
    array: output image
    """

    ax, ay = image.shape
    # normalize [a[0], a[1]] input intensities to [0,1]
    image = (image.clip(a[0], a[1]) - a[0]) / (a[1] - a[0])
    imi = np.zeros_like(image)
    for s in sigma:
        # B convolved by k_w
        bwi = scipy.ndimage.gaussian_filter(image, s, mode='nearest')
        # sigma_w
        swi = np.sqrt(scipy.ndimage.gaussian_filter(
            (image - bwi) ** 2, s, mode='nearest'))
        # intermediate sum of C'_i
        imi += np.arctan(k * (image - bwi) / swi)
    # weighted sum of gamma-transformed input image and normalized average of C'_i,
    # normalized to [b[0], b[1]]
    return b[0] + (b[1] - b[0]) * ((1. - w) * image ** (1. / gamma)
                                   + w * (.5 + imi / (len(sigma) * np.pi)))
