#!/usr/bin/env python

import pytest

import numpy as np

import sunpy.map
import sunpy.data.sample

from medocimage import mgn


def test_mgn():
    aiamap = sunpy.map.Map(sunpy.data.sample.AIA_171_IMAGE)
    dmgn = mgn(aiamap.data, a=(5., 5000.), b=(0., 1.), w=0.3, gamma=3.2, sigma=[2.5, 5, 10, 20, 40], k=0.7)
    assert (dmgn.shape == aiamap.data.shape)
    assert (np.nanmin (dmgn) >= 0.)
    assert (np.nanmax (dmgn) <= 1.)
